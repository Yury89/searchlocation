
public class User {
	
	private String userLogin;
	private String userPassword;
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	@Override
	public boolean equals(Object obj) {
		
		if(((User)obj).userLogin == this.userLogin)
			return true;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		return this.userLogin.hashCode();
	}
	
}
