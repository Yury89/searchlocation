
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAttributeChangeEvent;
import com.gargoylesoftware.htmlunit.html.HtmlAttributeChangeListener;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.gargoylesoftware.htmlunit.javascript.host.Window;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.gargoylesoftware.htmlunit.util.NameValuePair;


public class App 
{

    
    public static List<String> getUsersLocation(WebClient webclient,String userId){
    	List<String> locations = new ArrayList<String>();
    	HtmlPage userPage = null;
    	String currentCity;
    	String hometown;
		try {
			userPage = webclient.getPage("https://www.facebook.com/app_scoped_user_id/" + userId +"/");
		} catch (FailingHttpStatusCodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String mainUserUrl = userPage.executeJavaScript("window.location.href ").getJavaScriptResult().toString();
    	HtmlPage userLocationPage = null;
		try {
			userLocationPage = webclient.getPage(mainUserUrl + "/about?section=living&pnref=about");
		} catch (FailingHttpStatusCodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
    	 currentCity = userLocationPage.getElementById("current_city").asText().split("\n")[0];
		}
		catch(NullPointerException e){
			currentCity = "null";
		}
		try{
    	 hometown = userLocationPage.getElementById("hometown").asText().split("\n")[0];
		}
		catch(NullPointerException e) {
			hometown = "null";
		}
    	locations.add(currentCity);
    	locations.add(hometown);
    	return locations;
    	
    }
    
    
    public static WebClient authorization(User user) throws FailingHttpStatusCodeException, MalformedURLException, IOException{
    	WebClient webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER_11);
    	webClient.getOptions().setJavaScriptEnabled(true);
    	webClient.getOptions().setCssEnabled(true);
    	webClient.getCookieManager().setCookiesEnabled(true);
    	 webClient.getOptions().setRedirectEnabled(true);
         webClient.getOptions().setThrowExceptionOnScriptError(false);
    	List<String> usersLocation = new ArrayList<String>();
    	HtmlPage page1 = webClient.getPage("https://www.facebook.com/");
    	List<HtmlForm> listF = page1.getForms();
    	HtmlForm form = null;
    	for(int i=0; i<listF.size(); i++)
    	{
    	    if(listF.get(i).getId().equals("login_form"))
    	    {
    	        form = listF.get(i);
    	        }
    	}
    	HtmlTextInput uName = form.getInputByName("email");
    	HtmlPasswordInput passWord = form.getInputByName("pass");
    	HtmlElement button = form.getFirstByXPath("//input[@type='submit']");
    	uName.setValueAttribute(user.getUserLogin());
    	passWord.setValueAttribute(user.getUserPassword());
    	button.click();
    	return webClient;
    }
   
}
