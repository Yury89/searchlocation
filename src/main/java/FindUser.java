

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;

/**
 * Servlet implementation class FindUser
 */
public class FindUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Map<User,WebClient> history = new HashMap<User, WebClient>();
	
//       public FindUser() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
//    	   User u = new User();
//    	   u.setUserLogin("yura89peregudov@gmail.com");
//    	   u.setUserPassword("553191qqqQ");
//    	   history.put(u, App.authorization(u));
//       }
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User u = new User();
		u.setUserLogin(request.getParameter("login"));
		u.setUserPassword(request.getParameter("password"));
		if(!history.containsValue(u)){
			history.put(u, App.authorization(u));
		}
		response.setCharacterEncoding("UTF-8");
		response.getWriter().println(App.getUsersLocation(history.get(u), request.getParameter("id")));
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
